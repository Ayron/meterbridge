#include <math.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_thread.h>
#include <GL/gl.h>

#include "main.h"
#include "linedraw.h"
#include "find_image.h"
#include "gl_utils.h"

static float lp[MAX_METERS];
static SDL_Rect buf_rect[MAX_METERS];

static const float angle_scale = 15.7f;
static const float angle_offset = 0.7853975f;
static const float needle_len = 98.0f;
static const float needle_hub = 21.0f;

void load_graphics_vu()
{
    meter = find_image("vu-frame-small.png");
    if (!open_gl) {
	meter_buf = find_image("vu-frame-small.png");
    }
}

int gfx_thread_vu(void *foo)
{
    int x1, y1, x2, y2, i;
    const Uint32 needle = SDL_MapRGB(meter->format, 0x70, 0x70, 0x70);
    const Uint32 aa = SDL_MapRGB(meter->format, 0xC0, 0xC0, 0x60);
    float theta;

    for (i = 0; i < MAX_METERS; i++) {
	lp[i] = 0.0f;
	buf_rect[i].x = dest[i].x + 30;
	buf_rect[i].y = dest[i].y + 70;
	buf_rect[i].w = 168;
	buf_rect[i].h = 86;
    }

    while (1) {
	for (i = 0; i < num_meters; i++) {
	    lp[i] = lp[i] * 0.8f + env[i] * bias * 0.2f;
	    theta = (lp[i] * angle_scale) - angle_offset;

	    if (theta < -0.7853975f)
		theta = -0.7853975f;
	    if (theta > 0.7853975f)
		theta = 0.7853975f;

	    x1 = 108 + (int) (sinf(theta) * needle_hub);
	    y1 = 169 - (int) (cosf(theta) * needle_hub);
	    x2 = 108 + (int) (sinf(theta) * needle_len);
	    y2 = 169 - (int) (cosf(theta) * needle_len);

	    SDL_BlitSurface(meter, buf_rect, meter_buf, buf_rect);
	    draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
	    draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
	    SDL_BlitSurface(meter_buf, buf_rect, screen, buf_rect + i);
	}
	SDL_UpdateRects(screen, 1, &win);
	SDL_Delay(100);
    }

    return 0;
}

int gl_thread_vu(void *foo)
{
    GLuint texture[2];
    float frac_w, frac_h;
    int cols = 0, rows = 0;
    unsigned int x, y, i;
    float w, h, vs, hs, xo, yo;
    float scale = 1.0f;

    init_gl();

    glGenTextures(2, texture);

    make_texture(texture[0], background_image, NULL, NULL);
    make_texture(texture[1], meter, &frac_w, &frac_h);

    get_cols_rows(&cols, &rows);

    while (1) {
	if (win_resized()) {
	    resize_window();
	    scale = get_scale();

	    hs = win.w / (float)cols;
	    vs = win.h / (float)rows;
	    if (vs < hs) {
		w = h = vs;
		xo = (win.w / (float)cols - w) * 0.5f;
		yo = 0.0f;
	    } else {
		w = h = hs;
		xo = 0.0f;
		yo = (win.h / (float)rows - h) * 0.5f;
	    }
	}

	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();
	glColor3f(1.0, 1.0f, 1.0f);

	paint_background(texture[0]);

	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glBegin(GL_QUADS);
	for (y=0; y<rows; y++) {
	  for (x=0; x<cols; x++) {
	    glTexCoord2f(0.0f, 0.0f);
	    glVertex2f(xo + x * hs, yo + y * vs);
	    glTexCoord2f(frac_w, 0.0f);
	    glVertex2f(xo + x * hs + w, yo + y * vs);
	    glTexCoord2f(frac_w, frac_h);
	    glVertex2f(xo + x * hs + w, yo + y * vs + h);
	    glTexCoord2f(0.0f, frac_h);
	    glVertex2f(xo + x * hs, yo + y * vs + h);
	  }
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(0.5f, 0.5f, 0.5f);
	i = 0;
	for (y=0; y<rows; y++) {
	    for (x=0; x<cols; x++, i++) {
		float theta;

		lp[i] = lp[i] * 0.8f + env[i] * bias * 0.2f;
		theta = (lp[i] * angle_scale) - angle_offset;

		if (theta < -0.7853975f)
		    theta = -0.7853975f;
		if (theta > 0.7853975f)
		    theta = 0.7853975f;

		glLoadIdentity();
		glTranslatef(xo + x * hs + 109 * scale, yo + y * vs + 170 * scale, 0.0f);
		glScalef(scale, scale, 1.0f);
		glRotatef(theta * 57.29578f, 0.0f, 0.0f, 1.0f);

		glBegin(GL_LINES);
		    glVertex2f(-0.7f, -20.0f);
		    glVertex2f(-0.4f, -100.0f);
		    glVertex2f(-0.4f, -100.0f);
		    glVertex2f( 0.7f, -20.0f);
		    glVertex2f( 0.0f, -20.0f);
		    glVertex2f( 0.0f, -100.0f);
		glEnd();
	    }
	}

	SDL_GL_SwapBuffers();

	SDL_Delay(100);
    }
}

/* vi:set ts=8 sts=4 sw=4: */
