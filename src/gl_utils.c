/*
 *  Copyright (C) 2003 Steve Harris
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  $Id: gl_utils.c,v 1.2 2003/08/21 08:22:46 swh Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>
#include "SDL.h"
#include "main.h"
#include "gl_utils.h"

static volatile int pending_resize = 0;
static int video_flags = 0;
float resize_scale = 1.0f;

Uint32 getpixel(SDL_Surface *surface, int x, int y);

void make_texture(GLuint texture, SDL_Surface *surface, float *frac_w, float *frac_h)
{
    Uint8 *pixmap;
    unsigned int bound = 16;
    unsigned int x, y, pos;

    if (!surface) {
	fprintf(stderr, "No surface supplied\n");
	return;
    }

    glBindTexture(GL_TEXTURE_2D, texture);

    /* find minimal 2^n texture shape */
    while (bound < surface->w || bound < surface->h)
	bound <<= 1;

    if (frac_w) {
	*frac_w = (float)surface->w / bound;
    }
    if (frac_h) {
	*frac_h = (float)surface->h / bound;
    }

    pos = 0;
    if (surface->format->BytesPerPixel == 3) {
	pixmap = alloca(sizeof(Uint8) * bound * bound * 3);
	for (y=0; y<bound; y++) {
	    for (x=0; x<bound; x++) {
		Uint8 r, g, b;
		const Uint32 color = getpixel(surface, x, y);

		SDL_GetRGB(color, surface->format, &r, &g, &b);
		pixmap[pos++] = r;
		pixmap[pos++] = g;
		pixmap[pos++] = b;
	    }
	}
	glTexImage2D(GL_TEXTURE_2D, 0, 3, bound, bound, 0, GL_RGB,
		     GL_UNSIGNED_BYTE, pixmap);
    } else if (surface->format->BytesPerPixel == 4) {
	pixmap = alloca(sizeof(Uint8) * bound * bound * 4);
	for (y=0; y<bound; y++) {
	    for (x=0; x<bound; x++) {
		Uint8 r, g, b, a;
		const Uint32 color = getpixel(surface, x, y);

		SDL_GetRGBA(color, surface->format, &r, &g, &b, &a);
		pixmap[pos++] = r;
		pixmap[pos++] = g;
		pixmap[pos++] = b;
		pixmap[pos++] = a;
	    }
	}
	glTexImage2D(GL_TEXTURE_2D, 0, 4, bound, bound, 0, GL_RGBA,
		     GL_UNSIGNED_BYTE, pixmap);
    } else {
	fprintf(stderr, "Unknown surface format\n");
        return;
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *) surface->pixels + y * surface->pitch + x * bpp;

    if (x >= surface->w || y >= surface->h) return 0;

    switch (bpp) {
    case 1:
        return *p;

    case 2:
        return *(Uint16 *) p; 

    case 3:
        if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
        return *(Uint32 *) p;

    default:
        return 0;               /* shouldn't happen, but avoids warnings */
    }
}

SDL_Surface *init_gl()
{
    const SDL_VideoInfo *video_info;

    if (!(video_info = SDL_GetVideoInfo())) {
	fprintf(stderr, "Unable to get SDL info: %s\n", SDL_GetError());
	exit(1);
    }

    printf("Initialising OpenGL system\n");
    video_flags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
    video_flags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
    video_flags |= SDL_HWPALETTE;       /* Store the palette in hardware */
    video_flags |= SDL_RESIZABLE;       /* Enable window resizing */

    /* This checks to see if surfaces can be stored in memory */
    if (video_info->hw_available) {
	video_flags |= SDL_HWSURFACE;
    } else {
	video_flags |= SDL_SWSURFACE;
    }
    if (video_info->blit_hw) {
	video_flags |= SDL_HWACCEL;
    }
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    resize_window();
#if 0
    screen = SDL_SetVideoMode(win.w, win.h, SCREEN_BPP, video_flags);
    if (!screen) {
	fprintf(stderr, "Unable to set SDL video mode: %s\n", SDL_GetError());
	exit(1);
    }
#endif

    /* Turn on texturing */
    glEnable(GL_TEXTURE_2D);

    /* Set clear colour to back (doesn't really matter) */
    glClearColor(0.0f, 0.0f, 0.0f, 0.5f);

    /* Smooth lines */
    glHint(GL_LINE_SMOOTH, GL_NICEST);
    glEnable(GL_LINE_SMOOTH);

#if 0
    /* Smooth polys */
    glHint(GL_POLYGON_SMOOTH, GL_NICEST);
    glEnable(GL_POLYGON_SMOOTH);
#endif 

    /* Configure blending */
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    pending_resize = 1;

    return screen;
}

void request_resize(int w, int h)
{
    win.w = w;
    win.h = h;
    pending_resize = 1;
}

void resize_window()
{
    float hscale, vscale;
    int rows, cols;

    screen = SDL_SetVideoMode(win.w, win.h, SCREEN_BPP, video_flags);
    if (!screen) {
	fprintf(stderr, "Unable to set SDL video mode: %s\n", SDL_GetError());
	exit(1);
    }

    get_cols_rows(&cols, &rows);

    /* Set up projection */
    glViewport(0, 0, win.w, win.h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, win.w, win.h, 0.0, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    hscale = (float)win.w / (cols * meter->w);
    vscale = (float)win.h / (rows * meter->h);

    if (hscale < vscale) {
        resize_scale = hscale;
    } else{
        resize_scale = vscale;
    }
}

void paint_background(GLuint texture)
{
    glBindTexture(GL_TEXTURE_2D, texture);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2f(0.0f, 0.0f);
    glTexCoord2f(0.0f, win.h / 512.0f);
    glVertex2f(0.0f, win.h);
    glTexCoord2f(win.w / 512.0f, win.h / 512.0f);
    glVertex2f(win.w, win.h);
    glTexCoord2f(win.w / 512.0f, 0.0f);
    glVertex2f(win.w, 0.0f);
    glEnd();
}

int win_resized()
{
    if (pending_resize) {
        pending_resize = 0;
        return 1;
    }

    return 0;
}

/* vi:set ts=8 sts=4 sw=4: */
