#include <math.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_thread.h>
#include <GL/gl.h>

#include "main.h"
#include "linedraw.h"
#include "find_image.h"
#include "envelopes.h"
#include "gl_utils.h"

static float lp1[MAX_METERS];
static float lp2[MAX_METERS];
static SDL_Rect buf_rect[MAX_METERS];

static const float pi_4 = 0.785398f;
static const float needle_len = 82.0f;
static const float needle_hub = 15.0f;

void load_graphics_ppm()
{
    meter = find_image("ppm-frame-small.png");
    if (!open_gl) {
	meter_buf = find_image("ppm-frame-small.png");
    }
}

int gfx_thread_ppm(void *foo)
{
	int x1, y1, x2, y2, i;
	Uint32 needle, aa;
	float peak, theta;

	for (i=0; i<MAX_METERS; i++) {
		lp1[i] = 0.0f;
		buf_rect[i].x = dest[i].x + 23;
		buf_rect[i].y = dest[i].y + 55;
		buf_rect[i].w = 127;
		buf_rect[i].h = 84;
	}

	while (1) {
		for (i=0; i<num_meters; i++) {
			/* First meter: Mono and Stereo */
			if (((long)foo == 2) && (i % 2 == 0)) {
				/* Yellow needle: Difference - 6dB */
				peak = sumdiff_read(i + 1) / 2.0;

				if (peak * bias > lp2[i]) {
					lp2[i] = lp2[i] * 0.5f + peak * bias * 0.5f;
				} else {
					lp2[i] = lp2[i] * 0.9f + peak * bias * 0.1f;
				}

				theta = 1.09083f * log10f(lp2[i]);

				if (theta < -pi_4) theta = -pi_4;
				if (theta > pi_4) theta = pi_4;
				x1 = 89 + (int)(sinf(theta) * needle_hub);
				y1 = 138 - (int)(cosf(theta) * needle_hub);
				x2 = 89 + (int)(sinf(theta) * needle_len);
				y2 = 138 - (int)(cosf(theta) * needle_len);

				needle = SDL_MapRGB(meter->format, 0xD0, 0xD0, 0x00);
				aa = SDL_MapRGB(meter->format, 0x78, 0x78, 0x00);
				SDL_BlitSurface(meter, buf_rect, meter_buf, buf_rect);
				draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
				SDL_BlitSurface(meter_buf, buf_rect, screen, buf_rect+i);

				/* White needle: Sum - 6dB */
				peak = sumdiff_read(i) / 2.0;

				if (peak * bias > lp1[i]) {
					lp1[i] = lp1[i] * 0.5f + peak * bias * 0.5f;
				} else {
					lp1[i] = lp1[i] * 0.9f + peak * bias * 0.1f;
				}

				theta = 1.09083f * log10f(lp1[i]);

				if (theta < -pi_4) theta = -pi_4;
				if (theta > pi_4) theta = pi_4;
				x1 = 89 + (int)(sinf(theta) * needle_hub);
				y1 = 138 - (int)(cosf(theta) * needle_hub);
				x2 = 89 + (int)(sinf(theta) * needle_len);
				y2 = 138 - (int)(cosf(theta) * needle_len);

				needle = SDL_MapRGB(meter->format, 0xD0, 0xD0, 0xD0);
				aa = SDL_MapRGB(meter->format, 0x78, 0x78, 0x78);
				draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
				SDL_BlitSurface(meter_buf, buf_rect, screen, buf_rect+i);
			}

			/* Second meter: Right and left */
			if (((long)foo == 2) && (i % 2 == 1)) {
				/* Red needle: Left channel */
				peak = env_read(i - 1);

				if (peak * bias > lp2[i]) {
					lp2[i] = lp2[i] * 0.5f + peak * bias * 0.5f;
				} else {
					lp2[i] = lp2[i] * 0.9f + peak * bias * 0.1f;
				}

				theta = 1.09083f * log10f(lp2[i]);

				if (theta < -pi_4) theta = -pi_4;
				if (theta > pi_4) theta = pi_4;
				x1 = 89 + (int)(sinf(theta) * needle_hub);
				y1 = 138 - (int)(cosf(theta) * needle_hub);
				x2 = 89 + (int)(sinf(theta) * needle_len);
				y2 = 138 - (int)(cosf(theta) * needle_len);

				needle = SDL_MapRGB(meter->format, 0xD0, 0x00, 0x00);
				aa = SDL_MapRGB(meter->format, 0x78, 0x00, 0x00);
				SDL_BlitSurface(meter, buf_rect, meter_buf, buf_rect);
				draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
				SDL_BlitSurface(meter_buf, buf_rect, screen, buf_rect+i);

				/* Green needle: Right channel */
				peak = env_read(i);

				if (peak * bias > lp1[i]) {
					lp1[i] = lp1[i] * 0.5f + peak * bias * 0.5f;
				} else {
					lp1[i] = lp1[i] * 0.9f + peak * bias * 0.1f;
				}

				theta = 1.09083f * log10f(lp1[i]);

				if (theta < -pi_4) theta = -pi_4;
				if (theta > pi_4) theta = pi_4;
				x1 = 89 + (int)(sinf(theta) * needle_hub);
				y1 = 138 - (int)(cosf(theta) * needle_hub);
				x2 = 89 + (int)(sinf(theta) * needle_len);
				y2 = 138 - (int)(cosf(theta) * needle_len);

				needle = SDL_MapRGB(meter->format, 0x00, 0xD0, 0x00);
				aa = SDL_MapRGB(meter->format, 0x00, 0x78, 0x00);
				draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
				SDL_BlitSurface(meter_buf, buf_rect, screen, buf_rect+i);
			}

			/* Single meter */
		        if ((long)foo != 2) {
				peak = env_read(i);

				if (peak * bias > lp1[i]) {
					lp1[i] = lp1[i] * 0.5f + peak * bias * 0.5f;
				} else {
					lp1[i] = lp1[i] * 0.9f + peak * bias * 0.1f;
				}

				theta = 1.09083f * log10f(lp1[i]);

				if (theta < -pi_4) theta = -pi_4;
				if (theta > pi_4) theta = pi_4;
				x1 = 89 + (int)(sinf(theta) * needle_hub);
				y1 = 138 - (int)(cosf(theta) * needle_hub);
				x2 = 89 + (int)(sinf(theta) * needle_len);
				y2 = 138 - (int)(cosf(theta) * needle_len);

				needle = SDL_MapRGB(meter->format, 0xD0, 0xD0, 0xD0);
				aa = SDL_MapRGB(meter->format, 0x78, 0x78, 0x78);
				SDL_BlitSurface(meter, buf_rect, meter_buf, buf_rect);
				draw_ptr(meter_buf, x1, y1, x2, y2, needle, aa);
				SDL_BlitSurface(meter_buf, buf_rect, screen, buf_rect+i);
			}
		}
		SDL_UpdateRects(screen, 1, &win);
		SDL_Delay(100);
	}

	return 0;
}

int gl_thread_ppm(void *foo)
{
    GLuint texture[2];
    float frac_w, frac_h;
    int cols = 0, rows = 0;
    unsigned int x, y, i;
    float w, h, vs, hs, xo, yo;
    float scale = 1.0f;
    float theta, peak;

    init_gl();

    glGenTextures(2, texture);

    make_texture(texture[0], background_image, NULL, NULL);
    make_texture(texture[1], meter, &frac_w, &frac_h);

    get_cols_rows(&cols, &rows);

    while (1) {
	if (win_resized()) {
	    resize_window();
	    scale = get_scale();

	    hs = win.w / (float)cols;
	    vs = win.h / (float)rows;
	    if (vs < hs) {
		w = h = vs;
		xo = (win.w / (float)cols - w) * 0.5f;
		yo = 0.0f;
	    } else {
		w = h = hs;
		xo = 0.0f;
		yo = (win.h / (float)rows - h) * 0.5f;
	    }
	}

	glClear(GL_COLOR_BUFFER_BIT);

	glLoadIdentity();
	glColor3f(1.0, 1.0f, 1.0f);

	paint_background(texture[0]);

	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glBegin(GL_QUADS);
	for (y=0; y<rows; y++) {
	  for (x=0; x<cols; x++) {
	    glTexCoord2f(0.0f, 0.0f);
	    glVertex2f(xo + x * hs, yo + y * vs);
	    glTexCoord2f(frac_w, 0.0f);
	    glVertex2f(xo + x * hs + w, yo + y * vs);
	    glTexCoord2f(frac_w, frac_h);
	    glVertex2f(xo + x * hs + w, yo + y * vs + h);
	    glTexCoord2f(0.0f, frac_h);
	    glVertex2f(xo + x * hs, yo + y * vs + h);
	  }
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glColor3f(0.5f, 0.5f, 0.5f);
	i = 0;
	for (y=0; y<rows; y++) {
	    for (x=0; x<cols; x++, i++) {
	        /* First meter: Mono and Stereo */
		if (((long)foo == 2) && (i % 2 == 0)) {
		    /* Yellow needle: Difference - 6dB */
		    peak = sumdiff_read(i + 1) / 2.0;

		    if (peak * bias > lp2[i]) {
			lp2[i] = lp2[i] * 0.5f + peak * bias * 0.5f;
		    } else {
			lp2[i] = lp2[i] * 0.9f + peak * bias * 0.1f;
		    }
		    theta = 1.09083f * log10f(lp2[i]);

		    if (theta < -pi_4) theta = -pi_4;
		    if (theta > pi_4) theta = pi_4;

		    glColor3f(0.5f, 0.5f, 0.0f);
		    glLoadIdentity();
		    glTranslatef(xo + x * hs + 89 * scale, yo + y * vs + 138 * scale, 0.0f);
		    glScalef(scale, scale, 1.0f);
		    glRotatef(theta * 57.29578f, 0.0f, 0.0f, 1.0f);

		    glBegin(GL_LINES);
			glVertex2f(-0.7f, -needle_hub);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f( 0.7f, -needle_hub);
			glVertex2f( 0.0f, -needle_hub);
			glVertex2f( 0.0f, -needle_len);
		    glEnd();

		    /* White needle: Sum - 6dB */
		    peak = sumdiff_read(i) / 2.0;

		    if (peak * bias > lp1[i]) {
			lp1[i] = lp1[i] * 0.5f + peak * bias * 0.5f;
		    } else {
			lp1[i] = lp1[i] * 0.9f + peak * bias * 0.1f;
		    }
		    theta = 1.09083f * log10f(lp1[i]);

		    if (theta < -pi_4) theta = -pi_4;
		    if (theta > pi_4) theta = pi_4;

		    glColor3f(0.5f, 0.5f, 0.5f);
		    glLoadIdentity();
		    glTranslatef(xo + x * hs + 89 * scale, yo + y * vs + 138 * scale, 0.0f);
		    glScalef(scale, scale, 1.0f);
		    glRotatef(theta * 57.29578f, 0.0f, 0.0f, 1.0f);

		    glBegin(GL_LINES);
			glVertex2f(-0.7f, -needle_hub);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f( 0.7f, -needle_hub);
			glVertex2f( 0.0f, -needle_hub);
			glVertex2f( 0.0f, -needle_len);
		    glEnd();
		}

	        /* Second meter: Right and left */
		if (((long)foo == 2) && (i % 2 == 1)) {
		    /* Red needle: Left channel */
		    peak = env_read(i - 1);

		    if (peak * bias > lp2[i]) {
			lp2[i] = lp2[i] * 0.5f + peak * bias * 0.5f;
		    } else {
			lp2[i] = lp2[i] * 0.9f + peak * bias * 0.1f;
		    }
		    theta = 1.09083f * log10f(lp2[i]);

		    if (theta < -pi_4) theta = -pi_4;
		    if (theta > pi_4) theta = pi_4;

		    glColor3f(0.5f, 0.0f, 0.0f);
		    glLoadIdentity();
		    glTranslatef(xo + x * hs + 89 * scale, yo + y * vs + 138 * scale, 0.0f);
		    glScalef(scale, scale, 1.0f);
		    glRotatef(theta * 57.29578f, 0.0f, 0.0f, 1.0f);

		    glBegin(GL_LINES);
			glVertex2f(-0.7f, -needle_hub);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f( 0.7f, -needle_hub);
			glVertex2f( 0.0f, -needle_hub);
			glVertex2f( 0.0f, -needle_len);
		    glEnd();

		    /* Green needle: Right channel */
		    peak = env_read(i);

		    if (peak * bias > lp1[i]) {
			lp1[i] = lp1[i] * 0.5f + peak * bias * 0.5f;
		    } else {
			lp1[i] = lp1[i] * 0.9f + peak * bias * 0.1f;
		    }
		    theta = 1.09083f * log10f(lp1[i]);

		    if (theta < -pi_4) theta = -pi_4;
		    if (theta > pi_4) theta = pi_4;

		    glColor3f(0.0f, 0.5f, 0.0f);
		    glLoadIdentity();
		    glTranslatef(xo + x * hs + 89 * scale, yo + y * vs + 138 * scale, 0.0f);
		    glScalef(scale, scale, 1.0f);
		    glRotatef(theta * 57.29578f, 0.0f, 0.0f, 1.0f);

		    glBegin(GL_LINES);
			glVertex2f(-0.7f, -needle_hub);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f( 0.7f, -needle_hub);
			glVertex2f( 0.0f, -needle_hub);
			glVertex2f( 0.0f, -needle_len);
		    glEnd();
		}

		/* Single meter */
	        if ((long)foo != 2) {
		    peak = env_read(i);

		    if (peak * bias > lp1[i]) {
			lp1[i] = lp1[i] * 0.5f + peak * bias * 0.5f;
		    } else {
			lp1[i] = lp1[i] * 0.9f + peak * bias * 0.1f;
		    }
		    theta = 1.09083f * log10f(lp1[i]);

		    if (theta < -pi_4) theta = -pi_4;
		    if (theta > pi_4) theta = pi_4;

		    glColor3f(0.5f, 0.5f, 0.5f);
		    glLoadIdentity();
		    glTranslatef(xo + x * hs + 89 * scale, yo + y * vs + 138 * scale, 0.0f);
		    glScalef(scale, scale, 1.0f);
		    glRotatef(theta * 57.29578f, 0.0f, 0.0f, 1.0f);

		    glBegin(GL_LINES);
			glVertex2f(-0.7f, -needle_hub);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f(-0.4f, -needle_len);
			glVertex2f( 0.7f, -needle_hub);
			glVertex2f( 0.0f, -needle_hub);
			glVertex2f( 0.0f, -needle_len);
		    glEnd();
		}
	    }
	}

	SDL_GL_SwapBuffers();

	SDL_Delay(100);
    }
}

/* vi:set ts=8 sts=4 sw=4: */
