#ifndef GL_UTILS_H
#define GL_UTILS_H

#include <GL/gl.h>

void make_texture(GLuint texture, SDL_Surface *surface, float *frac_w, float
		  *frac_h);

SDL_Surface *init_gl();
void resize_window();
void request_resize(int w, int h);
void paint_background(GLuint texture);
int win_resized();
extern float resize_scale;

static inline float get_scale()
{
	return resize_scale;
}

#endif
