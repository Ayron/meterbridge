#ifndef MAIN_H
#define MAIN_H

#include <SDL.h>
#include <jack/jack.h>

#define MAX_METERS 32

#define SCREEN_BPP 32

extern SDL_Surface *screen, *background_image;
extern SDL_Surface *image, *meter, *meter_buf;
extern SDL_Rect win, dest[MAX_METERS];

extern jack_port_t *input_ports[MAX_METERS];
extern jack_port_t *output_ports[MAX_METERS];

extern int num_meters;
extern int num_scopes;
extern int meter_freeze;
extern int open_gl;
extern float env[MAX_METERS];
extern float sumdiff[MAX_METERS];

extern float bias;

void get_cols_rows(int *c, int *r);

#endif
